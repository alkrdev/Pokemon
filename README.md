# Pokemon

A web application built on Angular12, to showcase competences surrounding JavaScript, HTML, CSS and Web Frameworks, in connection with Accelerated Learning @ Experis Academy / Noroff.no

## Install

```
git clone https://gitlab.com/alkrdev/Pokemon.git
cd pokemon
npm install
```


## Usage

The following command will open a new Webpage in your browser at `localhost:4200`. Remember to use your React extentions

```
ng serve
```

## Contributors

-   [Rasmus Falk-Jensen (@Museberg)](@Museberg)
-   [Alexander Kristensen (@alkrdev)](@alkrdev)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

Noroff Accelerate, 2022.
