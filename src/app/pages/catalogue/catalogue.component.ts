import { Component, OnInit } from '@angular/core';
import {Pokemon} from "../../models/pokemon";
import {PokemonService} from "../../services/pokemon.service";
import {SmallPokemon} from "../../models/smallPokemon";
import { HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  private _error: string = "";
  get error(): string {
    return this._error;
  }
  get pokemons(): SmallPokemon[]{
    return this.pokemonService.pokemons;
  }
  get totalPokemons(): number{
    return this.pokemonService.totalPokemons;
  }
  page: number = 1;

  constructor(
    private readonly pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    this.pokemonService.getPokemons(12, 12 * this.page);
  }

  // Fetches the Pokemons from our service
  getPokemons(){
    this.pokemonService.getPokemons(12, 12 * this.page);
  }

  public showAllInfo(pokemonName: string): void{
    // Fetching Pokemon with all info from API
    this.pokemonService.getAllData(pokemonName)
      .subscribe((response: Pokemon) => {
        // Getting stats
        let stats = "Stats: ";
        for (const stat of response.stats){
          stats += stat.stat.name + ", ";
        }
        // Getting abilities
        let abilities = "\nAbilities: ";
        for (const ability of response.abilities){
          abilities += ability.ability.name + ", ";
        }
        alert(stats + abilities);
      })

  }

}
