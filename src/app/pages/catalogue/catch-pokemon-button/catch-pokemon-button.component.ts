import {Component, Input, OnInit} from '@angular/core';
import {PokemonCollectionService} from "../../../services/pokemon-collection.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Trainer} from "../../types/trainer";
import {TrainerService} from "../../../services/trainer.service";

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.scss']
})
export class CatchPokemonButtonComponent implements OnInit {
  public isCollected: boolean = false;

  @Input() pokemonName: string = "";

  get loading(): boolean{
    return this.collectionService.loading;
  }

  constructor(
    private readonly collectionService: PokemonCollectionService,
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
    // Inputs are resolved!
    this.isCollected = this.trainerService.inCollection(this.pokemonName);
  }

  onCatchClick(event: any): void{
    // Add Pokemon to caught pokemons
    event.stopPropagation();
    this.collectionService.updateCollection(this.pokemonName)
      .subscribe({
        next: (response: Trainer) => {
          this.isCollected = this.trainerService.inCollection(this.pokemonName);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR: ", error.message);
        }
      })

    event.preventDefault();
  }
}
