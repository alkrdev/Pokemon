import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/pages/types/trainer';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
  }

  // Runs when changes detected in data-bound properties
  ngOnChanges(): void {

  }

  // Runs after HTML has been rendered
  ngAfterViewInit(): void {

  }

  onSubmit(form: NgForm): void {
    this.loginService.loginTrainer(form.value.trainername)
      .subscribe({
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer;
          this.login.emit()
        },
        error: () => {

        }
      })

  }

}
