import {Pokemon} from "../../models/pokemon";

export type Trainer = {
    id?: Number,
    username: String,
    pokemon: string[]
}
