import { Component, OnInit } from '@angular/core';
import {TrainerService} from "../../services/trainer.service";
import {Trainer} from "../types/trainer";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  get trainer(): Trainer | undefined{
    return this.trainerService.trainer;
  }

  constructor(
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
  }

}
