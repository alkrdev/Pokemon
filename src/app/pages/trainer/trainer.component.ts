import { Component, OnInit } from '@angular/core';
import {TrainerService} from "../../services/trainer.service";
import {Pokemon} from "../../models/pokemon";
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  collectedPokemons: Pokemon[] = [];

  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    // Fetching all collected pokemon from API in order to display them
    if (this.trainerService.trainer?.pokemon){
      for (const pokemon of this.trainerService.trainer?.pokemon){
        this.pokemonService.getAllData(pokemon).subscribe((response:Pokemon) => {
          this.collectedPokemons.push(response);
        })
      }
    }


  }

}
