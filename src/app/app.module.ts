import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LandingComponent } from './pages/landing/landing.component';
import { CatalogueComponent } from './pages/catalogue/catalogue.component';
import { TrainerComponent } from './pages/trainer/trainer.component';
import { LoginComponent } from './pages/landing/components/login/login.component';
import { NgxPaginationModule } from "ngx-pagination";
import { CatchPokemonButtonComponent } from './pages/catalogue/catch-pokemon-button/catch-pokemon-button.component';
import { NavbarComponent } from './pages/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    CatalogueComponent,
    TrainerComponent,
    LoginComponent,
    CatchPokemonButtonComponent,
    NavbarComponent,
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        NgxPaginationModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
