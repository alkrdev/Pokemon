export interface SmallPokemon {
  name: string;
  url: string;
  id: number;
  pictureUrl: string;
}
