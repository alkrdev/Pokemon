export interface Pokemon {
  id: number;
  name: string;
  height: string;
  sprites: PokemonSprite;
  stats: PokemonStat[];
  abilities: Ability[]
}

export interface Ability {
  ability: HerpDerp
}

export interface HerpDerp {
  name: string
}

export interface PokemonSprite{
  front_default: string;
}

export interface PokemonStat {
  base_stat: number;
  effort: number;
  stat: Stat
}

export interface Stat {
  name: string
}
