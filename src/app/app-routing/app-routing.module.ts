import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from '../pages/landing/landing.component';
import { CatalogueComponent } from '../pages/catalogue/catalogue.component';
import { TrainerComponent } from '../pages/trainer/trainer.component';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
  },
  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [ AuthGuard ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }