import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Pokemon} from "../models/pokemon";
import {SmallPokemon} from "../models/smallPokemon";
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private _error: string = "";
  get error(): string{
    return this._error;
  }

  private _pokemons: SmallPokemon[] = [];
  get pokemons(): SmallPokemon[]{
    return this._pokemons;
  }

  private _totalPokemons: number = 0;
  get totalPokemons(): number{
    return this._totalPokemons
  }

  constructor(private readonly http: HttpClient) {
  }

  // Gets all Pokemons in accordance with the limit and offset
  getPokemons(limit: number, offset: number): void {
    this.http.get<SmallPokemon[]>(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .subscribe({
        next: (pokemons: any) => {
          this._pokemons = pokemons.results;
          this._totalPokemons = pokemons.count;

          for(const pok of this._pokemons){
            // Splitting url
            const splitUrl = pok.url.split('/');
            // Getting number on the end
            pok.id = parseInt(splitUrl.slice(-2).toString().replace(',',''));
            pok.pictureUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pok.id}.png`;
          }
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }

  // Gets all of the data related to a single Pokemon
  getAllData(name: string): Observable<Pokemon>{
    return this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${name}`)
  }
}
