import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Trainer } from '../pages/types/trainer';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
providedIn: 'root'
})
export class LoginService {
    private _httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'x-api-key': environment.API_KEY
        })
    };

    constructor(private readonly http: HttpClient) {
    }

    // Used on login form submit - If user exists, use it, otherwise create one.
    loginTrainer (trainername: String): Observable<Trainer> {
        return this.checkForTrainer(trainername)
            .pipe(
                switchMap((trainer: Trainer | undefined) => {
                    if (trainer === undefined) {
                        return this.createTrainer(trainername)
                    }
                    return of(trainer)
                })
            )
    };

    // Calls the api to check if a user exists and returns the user if it does
    checkForTrainer (trainername: String): Observable<Trainer | undefined> {
        const urlString = `${environment.BASE_URL}?username=${trainername}`

        return this.http.get<Trainer[]>(urlString)
            .pipe(map((response: Trainer[]) => response.pop()));
    };

    // Calls the API to create a user and returns the user
    createTrainer (trainername: String): Observable<Trainer> {
        let newTrainer: Trainer = {
            username: trainername,
            pokemon: []
        }

        return this.http.post<Trainer>(environment.BASE_URL, newTrainer, this._httpOptions)
    };
}
