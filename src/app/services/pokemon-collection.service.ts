import { Injectable } from '@angular/core';
import {PokemonService} from "./pokemon.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TrainerService} from "./trainer.service";
import {Pokemon} from "../models/pokemon";
import {Trainer} from "../pages/types/trainer";
import {finalize, Observable, tap} from "rxjs";

const { API_KEY, BASE_URL} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCollectionService {

  private _loading: boolean = false;
  get loading(): boolean{
    return this._loading;
  }

  constructor(
    private httpClient: HttpClient,
    private readonly pokemonService: PokemonService,
    private readonly trainerService: TrainerService
  ) { }

  public updateCollection(name: string): Observable<Trainer> {
    if (!this.trainerService.trainer){
      throw new Error("addToCollection: Trainer does not exists");
    }
    const trainer: Trainer | undefined = this.trainerService.trainer;
    // Getting pokemon of name from our API
    let pokemon: Pokemon | undefined = undefined;
    this.pokemonService.getAllData(name).subscribe((response:Pokemon) => {
      pokemon = response;
      console.log("Reponse: ", pokemon)
    });

    if (!pokemon){
      //throw new Error("addToCollection: No pokemon with name: " + name);
    }

    if (this.trainerService.inCollection(name)){
      this.trainerService.removeFromCollection(name);
    }
    else {
      this.trainerService.addToCollection(name);
    }

    const headers = new HttpHeaders({
      'ContentType': 'application/json',
      'x-api-key': API_KEY
    })

    this._loading = true;
    console.log("Existing pokemons: ", trainer.pokemon);

    return this.httpClient.patch<Trainer>(`${BASE_URL}/${trainer.id}`, {
      pokemon: [...trainer.pokemon] // Pokemon collection already updated, pushing to API
    },{
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(() => {
        this._loading = false;
      })
    );
  }
}
