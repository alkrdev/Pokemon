import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../pages/types/trainer';
import { StorageUtil } from '../utils/storage.utils';
import {Pokemon} from "../models/pokemon";

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.Save<Trainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.Read<Trainer>(StorageKeys.Trainer)
  }

  public inCollection(pokemonName: string): boolean{
    if (this.trainer){
      return Boolean(this.trainer?.pokemon.find((pokemon : string) => pokemon === pokemonName));
    }

    return false;
  }

  public removeFromCollection(pokemonName: string): void {
    if (this.trainer){
      this.trainer.pokemon = this.trainer.pokemon.
        filter((pokemon: string) => pokemon !== pokemonName);
    }
  }

  public addToCollection(pokemonName: string): void {
    if (this.trainer){
      this.trainer.pokemon.push(pokemonName);
    }
  }
}
